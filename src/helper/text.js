class TextHelper {

    static getNameFirstLetter(text) {

        let nameParts = text.split(' ')
        return (nameParts[0][0] + (nameParts[1] && nameParts[1][0] ? nameParts[1][0] : '')).toUpperCase()
    }

    static mask(number) {
        return number.substr(0, 3) + 'X'.repeat(number.length - 7) + number.substr(number.length - 4, 4)
    }

    static cleanSpecialCharacters(text) {
        if (!text || typeof text !== 'string') {
            return ''
        }
        return text.trim().toLowerCase().replace(/[^a-zA-Z0-9]+/g, '')
    }

    static cleanAccentCharacters(text) {
        
        let from = 'àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñç'
        let to = 'aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouunc'
        let result = text.trim().toLowerCase()
        
        for (var i = 0; i < from.length; i++) {
            result = result.replace(RegExp(from[i], 'gi'), to[i])
        }

        return result
    }
}

export default TextHelper