const appConfigs = {
    LOGGER: {
        REDUX: true,
    },
    API: {
        URL: {
            ROOT_URL: 'https://api-dev.telepro.me/api/v1'
        },
        END_POINT: {
            USER_INFO: ''
        }
    },
    REQUEST: {
        TIMEOUT: 120000
    },
    FACEBOOK: {
        APP_ID: '2076938309223251',
        ACCOUNT_KIT: {
            STATE: 'Workstay_CMS_Authentication',
            API_VERSION: 'v1.0',
            EVENT_ENABLE: true,
            REDIRECT_URL: '',
            DEBUG: false
        }
    },
    AUTHENTICATED_DATA: {
        EXPIRED_TIME: 365
    }
}

export default appConfigs