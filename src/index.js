import React from 'react'
import ReactDom from 'react-dom'
import Root from './root/'

if (window.location.protocol != 'https:' && window.location.hostname !== 'localhost') {
    window.location.href = 'https:' + window.location.href.substring(window.location.protocol.length)
}

ReactDom.render(<Root />, document.getElementById('root'))