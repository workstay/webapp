export const ROUTE_NAME_SPLASH = '/'
export const ROUTE_NAME_HELLO = '/hello'

export const ROUTE_NAME_AUTHENTICATION = 'authen'
export const ROUTE_NAME_AUTHENTICATION_SIGNIN = 'signin'
export const ROUTE_NAME_AUTHENTICATION_SIGNUP = 'signup'