export const COOKIES_KEY_INITIAL_URL = 'initial_url'
export const COOKIES_KEY_LANGUAGE = 'language'
export const COOKIES_KEY_THEME = 'theme'
export const COOKIES_KEY_TOKEN = 'token'
