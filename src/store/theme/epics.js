import {
    filter,
    map
} from 'rxjs/operators'

import * as actionTypes from './actionTypes'

export const changeThemeEpic = (action$) => (action$.pipe(
    filter((action) => (action.type === actionTypes.CHANGE_THEME_CALL)),
    map((action) => ({
        type: actionTypes.CHANGE_THEME_RETURN,
        payload: { ...action.payload }
    }))
))