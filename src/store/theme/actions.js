import * as actionTypes from './actionTypes.js'

export const changeTheme = (newTheme) => ({
    type: actionTypes.CHANGE_THEME_CALL,
    payload: {
        newTheme
    }
})