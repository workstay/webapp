import * as actionTypes from './actionTypes'
import * as appStateConstant from 'constant/appState'

const initalState = {
    initialized: appStateConstant.APP_STATE_INITIALIZED_NO
}

const appStateReducers = (state = initalState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_APP_STATE_INITIALIZED_RETURN:
            return { initialized: action.payload.newAppStateInitialized }
        default:
            return { ...state }
    }
}

export default appStateReducers