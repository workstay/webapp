import * as actionTypes from './actionTypes.js'

export const changeAuthenticatedData = (newAuthenticatedData) => ({
    type: actionTypes.CHANGE_AUTHENTICATED_DATA_CALL,
    payload: {
        newAuthenticatedData
    }
})