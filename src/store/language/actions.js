import * as actionTypes from './actionTypes.js'

export const changeLanguage = (newLanguage) => ({
    type: actionTypes.CHANGE_LANGUAGE_CALL,
    payload: {
        newLanguage
    }
})