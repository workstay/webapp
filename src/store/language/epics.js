import {
    filter,
    map
} from 'rxjs/operators'

import * as actionTypes from './actionTypes'

export const changeLanguageEpic = (action$) => (action$.pipe(
    filter((action) => (action.type === actionTypes.CHANGE_LANGUAGE_CALL)),
    map((action) => ({
        type: actionTypes.CHANGE_LANGUAGE_RETURN,
        payload: { ...action.payload }
    }))
))