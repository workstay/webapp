import * as languageConstant from 'constant/language'

import * as vn from './vn'

class Language {
    static renderDisplayText(language, limit, total) {
        switch (language) {
            case languageConstant.LANGUAGE_TYPE_VN:
                return `${vn.LANGUAGE_TEXT_PAGINATION_DISPLAY_TEXT_PART_1} ${limit} ${vn.LANGUAGE_TEXT_PAGINATION_DISPLAY_TEXT_PART_2} ${total}`;
            default:
                break;
        }
    }
}

export default Language