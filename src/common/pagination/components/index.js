import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Language from '../languages/'

class PaginationComponent extends React.Component {

    constructor(props) {
        super(props)
    }

    onChangePage(event, page) {

        event.preventDefault()
        this.props.onChangePage(page)
    }

    onChangeLimit(event) {
        event.preventDefault()
        this.props.onChangeLimit(parseInt(event.target.value))
    }

    componentDidMount() {
        import(`../styles/${this.props.theme}/index.css`)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.theme !== nextProps.theme) {
            import(`../styles/${this.props.theme}/index.css`)
        }
    }

    render() {
        return (
            <div className='row'>
                <div className='col-xl-12'>
                    <div className='kt-portlet'>
                        <div className='kt-portlet__body'>
                            <div className='kt-pagination kt-pagination--brand'>
                                <ul className='kt-pagination__links'>
                                    {
                                        this.props.page > 2 && (
                                            <li className='kt-pagination__link--first' onClick={(event) => this.onChangePage(event, 1)} >
                                                <a href='javascript:;'><i className='fa fa-angle-double-left kt-font-brand'></i></a>
                                            </li>
                                        )
                                    }
                                    {
                                        this.props.page > 1 && (
                                            <li className='kt-pagination__link--next' onClick={(event) => this.onChangePage(event, this.props.page - 1)}>
                                                <a href='javascript:;'><i className='fa fa-angle-left kt-font-brand'></i></a>
                                            </li>
                                        )
                                    }
                                    {
                                        this.props.page > 3 && (
                                            <li>
                                                <a href='javascript:;'>...</a>
                                            </li>
                                        )
                                    }
                                    {
                                        this.props.page > 2 && (
                                            <li onClick={(event) => this.onChangePage(event, this.props.page - 2)} >
                                                <a href='javascript:;'>{this.props.page - 2}</a>
                                            </li>
                                        )
                                    }
                                    {
                                        this.props.page > 1 && (
                                            <li onClick={(event) => this.onChangePage(event, this.props.page - 1)} >
                                                <a href='javascript:;'>{this.props.page - 1}</a>
                                            </li>
                                        )
                                    }
                                    <li className='kt-pagination__link--active'>
                                        <a href='javascript:;'>{this.props.page}</a>
                                    </li>
                                    {
                                        Math.ceil(this.props.total / this.props.limit) - this.props.page > 0 && (
                                            <li onClick={(event) => this.onChangePage(event, this.props.page + 1)} >
                                                <a href='javascript:;'>{this.props.page + 1}</a>
                                            </li>
                                        )
                                    }
                                    {
                                        Math.ceil(this.props.total / this.props.limit) - this.props.page > 1 && (
                                            <li onClick={(event) => this.onChangePage(event, this.props.page + 2)} >
                                                <a href='javascript:;'>{this.props.page + 2}</a>
                                            </li>
                                        )
                                    }
                                    {
                                        Math.ceil(this.props.total / this.props.limit) - this.props.page > 2 && (
                                            <li onClick={(event) => this.onChangePage(event, this.props.page + 3)} >
                                                <a href='javascript:;'>{this.props.page + 3}</a>
                                            </li>
                                        )
                                    }
                                    {
                                        Math.ceil(this.props.total / this.props.limit) - this.props.page > 3 && (
                                            <li>
                                                <a href='javascript:;'>...</a>
                                            </li>
                                        )
                                    }
                                    {
                                        Math.ceil(this.props.total / this.props.limit) - this.props.page > 0 && (
                                            <li className='kt-pagination__link--prev' onClick={(event) => this.onChangePage(event, this.props.page + 1)} >
                                                <a href='javascript:;'><i className='fa fa-angle-right kt-font-brand'></i></a>
                                            </li>
                                        )
                                    }
                                    {
                                        Math.ceil(this.props.total / this.props.limit) - this.props.page > 1 && (
                                            <li className='kt-pagination__link--last' onClick={(event) => this.onChangePage(event, Math.ceil(this.props.total / this.props.limit))} >
                                                <a href='javascript:;'><i className='fa fa-angle-double-right kt-font-brand'></i></a>
                                            </li>
                                        )
                                    }
                                </ul>
                                <div className='kt-pagination__toolbar'>
                                    <select className='form-control kt-font-brand' style={{ width: 60 }} value={this.props.limit} onChange={(event) => this.onChangeLimit(event)} >
                                        {
                                            this.props.limits.map((limit, index) => {
                                                return (
                                                    <option key={`pagination_limit_${index}`} value={limit}>{limit}</option>
                                                )
                                            })
                                        }
                                    </select>
                                    <span className='pagination__desc'>
                                        {Language.renderDisplayText(this.props.language, this.props.limit > this.props.total ? this.props.total : this.props.limit, this.props.total)}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    language: state.appLanguage.current,
    theme: state.appTheme.current
})

const mapDispatchToProps = (dispatch) => ({

})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PaginationComponent))