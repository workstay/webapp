import React from 'react'
import PaginationComponent from 'common/pagination/components/'
import Language from '../languages'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import * as DataConstant from 'constant/data'
import Scroll from 'helper/scroll'

class CommonTableComponent extends React.Component {
    componentDidMount() {
        Scroll.init('#tableWrapper')
    }

    render() {
        return <>
            {
                this.props.authenticatedData && <div className='text-nowrap position-relative' id='tableWrapper' style={{overflow: 'visible',...this.props.styles}}>
                    {
                        this.props.data.length === 0 && this.props.authenticatedData.user ?
                            <div className='d-flex align-items-center justify-content-center' style={{ minHeight: 200 }}>
                                <h3 className='kt-font-bold'>
                                    {
                                        Language.renderNoDataText(this.props.language)
                                    }
                                </h3>
                            </div> :
                            <table className='table table-striped table-bordered' id='table' style={this.props.tableStyles}>
                                <thead className='thead-dark'>
                                    <tr className=''>
                                        {
                                            this.props.options
                                                .filter(item => item.noAgentAllowed ? this.props.authenticatedData.user.type !== DataConstant.USER_TYPE_AGENT : true)
                                                .map(item => <th className={item.thClass} style={item.thStyle} key={item.key}>{item.th}</th>)
                                        }
                                    </tr>
                                </thead>
                                <tbody className=''>
                                    {
                                        this.props.data.map((item, index) => <tr className='' key={`tablerow-${index}`}>
                                            {
                                                this.props.options
                                                    .filter(item => item.noAgentAllowed ? this.props.authenticatedData.user.type !== DataConstant.USER_TYPE_AGENT : true)
                                                    .map(option => <td
                                                        className={typeof option.tdClass === 'function' ? option.tdClass(item) : option.tdClass}
                                                        style={typeof option.tdStyle === 'function' ? option.tdStyle(item) : option.tdStyle}
                                                        key={`${option.key}-${index}`}>{typeof option.td === 'function' ? option.td(item, index) : option.td}</td>)
                                            }
                                        </tr>)
                                    }
                                </tbody>

                            </table>
                    }
                </div>
            }
            {
                this.props.pagination && <PaginationComponent
                    limits={[10, 20, 50, 100]}
                    page={this.props.pagination.page}
                    limit={this.props.pagination.limit}
                    total={this.props.pagination.total}
                    onChangePage={(page) =>
                        this.props.onChangePage(page)
                    }
                    onChangeLimit={(limit) =>
                        this.props.onChangeLimit(limit)
                    }
                />
            }
        </>
    }
}
const mapStateToProps = (state) => ({
    language: state.appLanguage.current,
    theme: state.appTheme.current,
    authenticatedData: state.appAuthentication.current,
});

const mapDispatchToProps = (dispatch) => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CommonTableComponent));