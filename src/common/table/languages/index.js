import * as languageConstant from 'constant/language';

import * as vn from './vn';

class Language {
    static renderNoDataText(language) {
        switch (language) {
            case languageConstant.LANGUAGE_TYPE_VN:
                return vn.LANGUAGE_TEXT_NO_DATA_TEXT;
            default:
                break;
        }
    };
};

export default Language;