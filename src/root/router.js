import React from 'react'
import { Redirect } from 'react-router'

import * as routeConstant from 'constant/route'

import DynamicImport from 'common/dynamicImport/components'

const SplashComponent = (props) => (
    <DynamicImport
        load={() => import('../modules/splash/components/')}
    >
        {(Component) => Component === null ? null : <Component {...props} />}
    </DynamicImport>
)

const HelloComponent = (props) => (
    <DynamicImport
        load={() => import('../modules/hello/components/')}
    >
        {(Component) => Component === null ? null : <Component {...props} />}
    </DynamicImport>
)

const AuthenticationComponent = (props) => (
    <DynamicImport
        load={() => import('../modules/authentication/components/')}
    >
        {(Component) => Component === null ? null : <Component {...props} />}
    </DynamicImport>
)



const AppRouter = [{
    path: routeConstant.ROUTE_NAME_SPLASH,
    exact: true,
    component: SplashComponent
},{
    path: routeConstant.ROUTE_NAME_HELLO,
    exact: true,
    component: ErrorComponent
}, {
    path: `/${routeConstant.ROUTE_NAME_AUTHENTICATION}/:page(${routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNIN}|${routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNUP}|${routeConstant.ROUTE_NAME_AUTHENTICATION_FORGOT}|)`,
    exact: true,
    component: AuthenticationComponent
}, {
    path: '*',
    component: () => <Redirect to={routeConstant.ROUTE_NAME_SPLASH} />
}]

export default AppRouter