import * as appStateConstant from 'constant/appState'
import * as routeConstant from 'constant/route'
import * as userConstant from 'constant/user'
import * as cookiesConstant from 'constant/cookies'

import Cookies from 'helper/cookies'

class AccessControl {

    constructor(store, browserHistory) {
        this.store = store
        this.browserHistory = browserHistory
    }

    routeToDefault(userType) {
        switch (userType) {
            case userConstant.USER_TYPE_ADMIN:
                this.browserHistory.replace(`/${routeConstant.ROUTE_NAME_MAIN}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD_ADMIN}`)
                break
            case userConstant.USER_TYPE_ENTERPRISE:
                this.browserHistory.replace(`/${routeConstant.ROUTE_NAME_MAIN}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD_ENTERPRISE}`)
                break
            case userConstant.USER_TYPE_AGENT:
                this.browserHistory.replace(`/${routeConstant.ROUTE_NAME_MAIN}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD_AGENT}`)
                break
            default:
                break
        }
    }

    hasAccessDashboard(pathname) {
        
        let state = this.store.getState()
        if (pathname.indexOf(`/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD_ADMIN}`) > -1) {
            return [userConstant.USER_TYPE_ADMIN].indexOf(state.appAuthentication.current.user.type) > -1
        }
        if (pathname.indexOf(`/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD_ENTERPRISE}`) > -1) {
            return [userConstant.USER_TYPE_ENTERPRISE].indexOf(state.appAuthentication.current.user.type) > -1
        }
        if (pathname.indexOf(`/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD_AGENT}`) > -1) {
            return [userConstant.USER_TYPE_AGENT].indexOf(state.appAuthentication.current.user.type) > -1
        }
        return true
    }

    checkRoute(pathname) {

        let state = this.store.getState()

        if (pathname !== routeConstant.ROUTE_NAME_SPLASH && state.appState.initialized !== appStateConstant.APP_STATE_INITIALIZED_YES) {
            Cookies.set(cookiesConstant.COOKIES_KEY_INITIAL_URL, pathname)
            this.browserHistory.push(`${routeConstant.ROUTE_NAME_SPLASH}`)
            return
        }

        if (pathname.indexOf(routeConstant.ROUTE_NAME_AUTHENTICATION) > -1 && state.appAuthentication.current) {
            this.browserHistory.push(`/${routeConstant.ROUTE_NAME_MAIN}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD}`)
            return
        }

        if (pathname.indexOf(routeConstant.ROUTE_NAME_MAIN) > -1 && !state.appAuthentication.current) {
            this.browserHistory.push(`/${routeConstant.ROUTE_NAME_AUTHENTICATION}/${routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNIN}`)
            return
        }

        if (pathname.indexOf(routeConstant.ROUTE_NAME_MAIN) > -1 && state.appAuthentication.current.user.type === userConstant.USER_TYPE_AGENT) {
            // code
        }
    }
}

export default AccessControl