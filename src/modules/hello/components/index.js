import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Language from '../languages/'
import Style from '../styles/'

class HelloComponent extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <style>{Style[this.props.theme]}</style>
                <p className='hello-text'>
                    {Language.Hello(this.props.language)}
                </p>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    language: state.appLanguage.current,
    theme: state.appTheme.current
})

const mapDispatchToProps = (dispatch) => ({

})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HelloComponent))