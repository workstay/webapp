import * as languageConstant from 'constant/language'

import * as vn from './vn'

class Language {

    static Hello(language) {
        switch (language) {
            case languageConstant.LANGUAGE_TYPE_VN:
                return vn.TEXT_HELLO
            default:
                break
        }
    }
}

export default Language