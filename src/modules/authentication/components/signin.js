import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Language from '../languages/';

import appConfigs from 'config';

import * as routeConstant from 'constant/route';
import * as facebookAccountKitConstant from 'constant/facebookAccountKit';
import * as cookiesConstant from 'constant/cookies';

import { changeAuthenticatedData } from 'store/authentication/actions';

import PageMessage from 'helper/pageMessage';
import BlockUI from 'helper/blockUI';
import Validator from 'helper/validator';
import HTTPRequest from 'helper/httpRequest';
import Cookies from 'helper/cookies';
import FacebookAccountKit from 'helper/facebookAccountKit';

class SigninComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputs: {
                username: '',
                password: ''
            },
            controls: {
                signingin: false
            }
        };

        this.signinFormValidator = null;
    };

    onInputsChanged(field, event) {
        this.setState({
            inputs: Object.assign({}, this.state.inputs, {
                [field]: event.target.value
            })
        });
    };

    onToSignupLinkClicked(event) {
        event.preventDefault();
        this.props.history.push(`/${routeConstant.ROUTE_NAME_AUTHENTICATION}/${routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNUP}`);
    };
    onToForgotLinkClicked(event) {
        event.preventDefault();
        this.props.history.push(`/${routeConstant.ROUTE_NAME_AUTHENTICATION}/${routeConstant.ROUTE_NAME_AUTHENTICATION_FORGOT}`);
    };

    onSubmitButtonClicked(event) {
        if (event) {
            event.preventDefault();
        }

        if (!Validator.checkValid('#authentication_form_signin')) {
            return;
        }

        this.setState({
            controls: Object.assign({}, this.state.controls, { signingin: true })
        });
        BlockUI.blockPage({
            message: Language.renderSigninLoadingText(this.props.language)
        });

        HTTPRequest.post({
            url: 'auth',
            data: {
                username: this.state.inputs.username,
                password: this.state.inputs.password,
                device_id: 'web'
            }
        }).then((response) => {

            if (!response.data.success) {
                PageMessage.showError({
                    title: Language.renderSigninErrorGeneral(this.props.language)
                });
                return;
            }

            Cookies.set(cookiesConstant.COOKIES_KEY_TOKEN, response.data.token, { expires: appConfigs.AUTHENTICATED_DATA.EXPIRED_TIME });
            this.props.changeAuthenticatedData({
                token: response.data.token,
                user: response.data.user
            });
            setTimeout(() => {
                this.props.history.push(`/${routeConstant.ROUTE_NAME_MAIN}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD}`);
            }, 0);
        }).catch((error) => {
            if (error.response && error.response.status === 404) {
                PageMessage.showError({
                    title: Language.renderSigninErrorAccount(this.props.language)
                });
                return;
            }
            PageMessage.showError({
                title: Language.renderSigninErrorGeneral(this.props.language)
            });
        }).finally(() => {
            this.setState({
                controls: Object.assign({}, this.state.controls, { signingin: false })
            });
            BlockUI.unblockPage();
        });
    };
    onSigninOptionsPhoneNumbersClicked(event) {
        event.preventDefault();
        FacebookAccountKit.loginBySMS().then((code) => {

            this.setState({
                controls: Object.assign({}, this.state.controls, { signingin: true })
            });
            BlockUI.blockPage({
                message: Language.renderSigninLoadingText(this.props.language)
            });

            HTTPRequest.post({
                url: 'account_kit',
                data: {
                    code: code,
                    device_id: 'web'
                }
            }).then((response) => {

                if (!response.data.success) {
                    PageMessage.showError({
                        title: Language.renderSigninErrorGeneral(this.props.language)
                    });
                    return;
                }

                Cookies.set(cookiesConstant.COOKIES_KEY_TOKEN, response.data.token, { expires: appConfigs.AUTHENTICATED_DATA.EXPIRED_TIME });
                this.props.changeAuthenticatedData({
                    token: response.data.token,
                    user: response.data.user
                });
                setTimeout(() => {
                    this.props.history.push(`/${routeConstant.ROUTE_NAME_MAIN}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD}`);
                }, 0);
            }).catch((error) => {
                PageMessage.showError({
                    title: Language.renderSigninErrorGeneral(this.props.language)
                });
            }).finally(() => {
                this.setState({
                    controls: Object.assign({}, this.state.controls, { signingin: false })
                });
                BlockUI.unblockPage();
            });
        }).catch((error) => {
            if (error === facebookAccountKitConstant.FACEBOOK_ACCOUNT_KIT_RESPONSE_CODE_NOT_AUTHENTICATED) {
                return;
            }

            PageMessage.showError({
                title: Language.renderSigninErrorGeneral(this.props.language)
            });
        });
    };

    componentDidMount() {
        this.signinFormValidator = Validator.init('#authentication_form_signin', {
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                username: {
                    required: Language.renderSigninRequireUsername(this.props.language)
                },
                password: {
                    required: Language.renderSigninRequirePassword(this.props.language)
                }
            }
        });
    };
    componentWillUnmount() {
        this.signinFormValidator.destroy();
    };

    render() {
        return (
            <div className='kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper'>
                <div className='kt-login__head'>
                    <span className='kt-login__signup-label'>{Language.renderSigninRegisterHook(this.props.language)}</span>&nbsp;&nbsp;
                    <a href='javascript:;' className='kt-link kt-login__signup-link' onClick={(event) => this.onToSignupLinkClicked(event)}>
                        {Language.renderSigninRegisterTitle(this.props.language)}
                    </a>
                </div>
                <div className='kt-login__body flipInX animated'>
                    <div className='kt-login__form'>
                        <div className='kt-login__title'>
                            <h3>{Language.renderSigninTitle(this.props.language)}</h3>
                        </div>
                        <form className='kt-form' action='' noValidate='novalidate' id='authentication_form_signin'>
                            <div className='form-group'>
                                <input className='form-control' type='text' placeholder={Language.renderSigninFormPlaceholderUsername(this.props.language)} name='username' autoComplete='off' value={this.state.inputs.username} onChange={(event) => this.onInputsChanged('username', event)} />
                            </div>
                            <div className='form-group'>
                                <input className='form-control' type='password' placeholder={Language.renderSigninFormPlaceholderPassword(this.props.language)} name='password' value={this.state.inputs.password} onChange={(event) => this.onInputsChanged('password', event)} />
                            </div>
                            <div className='kt-login__actions'>
                                <a href='javascript:;' className='kt-link kt-login__link-forgot' onClick={(event) => this.onToForgotLinkClicked(event)}>
                                    {Language.renderSigninForgotTitle(this.props.language)}
                                </a>
                                <button id='kt_login_signin_submit' className='btn btn-primary btn-elevate kt-login__btn-primary' onClick={(event) => this.onSubmitButtonClicked(event)}>
                                    {Language.renderSigninSubmitTitle(this.props.language)}
                                </button>
                            </div>
                        </form>
                        <div className='kt-login__divider'>
                            <div className='kt-divider'>
                                <span></span>
                                <span>{Language.renderSigninOptionsHook(this.props.language)}</span>
                                <span></span>
                            </div>
                        </div>
                        <div className='kt-login__options'>
                            <a href='javascript:;' className='btn btn-secondary kt-btn' onClick={(event) => this.onSigninOptionsPhoneNumbersClicked(event)}>
                                <i className='fa fa-mobile-alt'></i>
                                {Language.renderSigninOptionsPhoneNumber(this.props.language)}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
};

const mapStateToProps = (state) => ({
    language: state.appLanguage.current,
    theme: state.appTheme.current
});

const mapDispatchToProps = (dispatch) => ({
    changeAuthenticatedData: (newAuthenticatedData) => dispatch(changeAuthenticatedData(newAuthenticatedData))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SigninComponent));
