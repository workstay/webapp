import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import appConfigs from 'config';

import * as routeConstant from 'constant/route';

import Language from '../languages/';

class InformationComponent extends React.Component {
    constructor(props) {
        super(props);
    };

    onChangeRouteClicked(event, pathname) {
        event.preventDefault();
        this.props.history.push(pathname);
    };

    render() {
        return (
            <div className='kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside' style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/assets/libs/metronic/media/bg/bg-4.jpg)` }}>
                <div className='kt-grid__item'>
                    <a href='javascript:;' className='kt-login__logo' onClick={(event) => this.onChangeRouteClicked(event, `/${routeConstant.ROUTE_NAME_AUTHENTICATION}/${routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNIN}`)}>
                        <img src={`${process.env.PUBLIC_URL}/assets/images/logo.png`} />
                    </a>
                </div>
                <div className='kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver'>
                    <div className='kt-grid__item kt-grid__item--middle'>
                        <h3 className='kt-login__title'>{Language.renderInformationTitle(this.props.language)}</h3>
                        <h4 className='kt-login__subtitle'>{Language.renderInformationHook(this.props.language)}</h4>
                    </div>
                </div>
                <div className='kt-grid__item'>
                    <div className='kt-login__info'>
                        <div className='kt-login__copyright'>
                            {Language.renderInformationCredit(this.props.language)}
                        </div>
                        <div className='kt-login__menu'>
                            <a href={appConfigs.EXTERNAL_LINKS.TELEPRO_PRIVACY} className='kt-link'>{Language.renderInformationPrivacy(this.props.language)}</a>
                            <a href={appConfigs.EXTERNAL_LINKS.TELEPRO_LEGAL} className='kt-link'>{Language.renderInformationLegal(this.props.language)}</a>
                            <a href={appConfigs.EXTERNAL_LINKS.TELEPRO_CONTACT} className='kt-link'>{Language.renderInformationContact(this.props.language)}</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
};

const mapStateToProps = (state) => ({
    language: state.appLanguage.current,
    theme: state.appTheme.current
});

const mapDispatchToProps = (dispatch) => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InformationComponent));
