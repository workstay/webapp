import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import appConfigs from 'config';

import Language from '../languages/';

import * as routeConstant from 'constant/route';
import * as userConstant from 'constant/user';
import * as cookiesConstant from 'constant/cookies';

import { changeAuthenticatedData } from 'store/authentication/actions';

import PageMessage from 'helper/pageMessage';
import BlockUI from 'helper/blockUI';
import Validator from 'helper/validator';
import HTTPRequest from 'helper/httpRequest';
import Cookies from 'helper/cookies';

class SignupComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputs: {
                type: '',
                username: '',
                email: '',
                mobile: '',
                password: '',
                repassword: ''
            },
            controls: {
                signingup: false
            }
        };

        this.signupFormValidator = null;
    };

    onInputsChanged(field, event) {
        this.setState({
            inputs: Object.assign({}, this.state.inputs, {
                [field]: event.target.value
            })
        });
    };

    onToSigninLinkClicked(event) {
        event.preventDefault();
        this.props.history.push(`/${routeConstant.ROUTE_NAME_AUTHENTICATION}/${routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNIN}`);
    };

    onSubmitButtonPressed(event) {
        event.preventDefault();

        if (!Validator.checkValid('#authentication_form_signup')) {
            return;
        }

        this.setState({
            controls: Object.assign({}, this.state.controls, { signingup: true })
        });
        BlockUI.blockPage({
            message: Language.renderSignupLoadingText(this.props.language)
        });

        HTTPRequest.post({
            url: 'register',
            data: {
                type: parseInt(this.state.inputs.type),
                username: this.state.inputs.username,
                email: this.state.inputs.email,
                mobile: this.state.inputs.mobile,
                password: this.state.inputs.password,
                device_id: 'web'
            }
        }).then((response) => {
            if (!response.data.success) {
                PageMessage.showError({
                    title: Language.renderSignupErrorGeneral(this.props.language)
                });
                return;
            }

            Cookies.set(cookiesConstant.COOKIES_KEY_TOKEN, response.data.token, { expires: appConfigs.AUTHENTICATED_DATA.EXPIRED_TIME });
            this.props.changeAuthenticatedData({
                token: response.data.token,
                user: response.data.user
            });
            setTimeout(() => {
                this.props.history.push(`/${routeConstant.ROUTE_NAME_MAIN}/${routeConstant.ROUTE_NAME_MAIN_DASHBOARD}`);
            }, 0);
        }).catch((error) => {
            if (error.response && error.response.status === 422) {
                PageMessage.showError({
                    title: Language.renderSignupErrorAccount(this.props.language)
                });
                return;
            }
            PageMessage.showError({
                title: Language.renderSignupErrorGeneral(this.props.language)
            });
        }).finally(() => {
            this.setState({
                controls: Object.assign({}, this.state.controls, { signingup: false })
            });
            BlockUI.unblockPage();
        });
    };

    componentDidMount() {
        this.signupFormValidator = Validator.init('#authentication_form_signup', {
            rules: {
                type: {
                    required: true
                },
                username: {
                    required: true,
                    maxlength: 32,
                    minlength: 6,
                    username: true
                },
                email: {
                    required: true,
                    email: true
                },
                mobile: {
                    required: true,
                    phone: true
                },
                password: {
                    required: true,
                    maxlength: 32,
                    minlength: 6
                },
                repassword: {
                    required: true,
                    equalTo: '#authentication_form_signup_field_password'
                }
            },
            messages: {
                type: {
                    required: Language.renderSignupRequireType(this.props.language)
                },
                username: {
                    required: Language.renderSignupRequireUsername(this.props.language),
                    maxlength: Language.renderSignupRangeUsername(this.props.language),
                    minlength: Language.renderSignupRangeUsername(this.props.language),
                    username: Language.renderSignupInvalidUsername(this.props.language)
                },
                email: {
                    required: Language.renderSignupRequireEmail(this.props.language),
                    email: Language.renderSignupInvalidEmail(this.props.language)
                },
                mobile: {
                    required: Language.renderSignupRequireMobile(this.props.language),
                    phone: Language.renderSignupInvalidMobile(this.props.language)
                },
                password: {
                    required: Language.renderSignupRequirePassword(this.props.language),
                    maxlength: Language.renderSignupRangePassword(this.props.language),
                    minlength: Language.renderSignupRangePassword(this.props.language)
                },
                repassword: {
                    required: Language.renderSignupRequireRepassword(this.props.language),
                    equalTo: Language.renderSignupNotEqualRepassword(this.props.language)
                }
            }
        });
    };
    componentWillUnmount() {
        this.signupFormValidator.destroy();
    };

    render() {
        return (
            <div className='kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper'>
                <div className='kt-login__body flipInX animated'>
                    <div className='kt-login__form'>
                        <div className='kt-login__title'>
                            <h3>{Language.renderSignupTitle(this.props.language)}</h3>
                        </div>
                        <form className='kt-form' action='' noValidate='novalidate' id='authentication_form_signup'>
                            <div className='form-group'>
                                <select className='form-control' name='type' value={this.state.inputs.type} onChange={(event) => this.onInputsChanged('type', event)}>
                                    <option value=''>{Language.renderSignupFormOptionTypeDefault(this.props.language)}</option>
                                    <option value={userConstant.USER_TYPE_ENTERPRISE}>{Language.renderSignupFormOptionTypeCompany(this.props.language)}</option>
                                    <option value={userConstant.USER_TYPE_AGENT}>{Language.renderSignupFormOptionTypeAgent(this.props.language)}</option>
                                </select>
                            </div>
                            <div className='form-group'>
                                <input className='form-control' type='text' placeholder={Language.renderSignupFormPlaceholderUsername(this.props.language)} name='username' autoComplete='off' value={this.state.inputs.username} onChange={(event) => this.onInputsChanged('username', event)} />
                            </div>
                            <div className='form-group'>
                                <input className='form-control' type='text' placeholder={Language.renderSignupFormPlaceholderEmail(this.props.language)} name='email' autoComplete='off' value={this.state.inputs.email} onChange={(event) => this.onInputsChanged('email', event)} />
                            </div>
                            <div className='form-group'>
                                <input className='form-control' type='text' placeholder={Language.renderSignupFormPlaceholderMobile(this.props.language)} name='mobile' autoComplete='off' value={this.state.inputs.mobile} onChange={(event) => this.onInputsChanged('mobile', event)} />
                            </div>
                            <div className='form-group'>
                                <input className='form-control' id='authentication_form_signup_field_password' type='password' placeholder={Language.renderSignupFormPlaceholderPassword(this.props.language)} name='password' value={this.state.inputs.password} onChange={(event) => this.onInputsChanged('password', event)} />
                            </div>
                            <div className='form-group'>
                                <input className='form-control' type='password' placeholder={Language.renderSignupFormPlaceholderRepassword(this.props.language)} name='repassword' value={this.state.inputs.repassword} onChange={(event) => this.onInputsChanged('repassword', event)} />
                            </div>
                            <div className='kt-login__actions'>
                                <a href='javascript:;' className='kt-link kt-login__link-forgot' onClick={(event) => this.onToSigninLinkClicked(event)}>
                                    {Language.renderSignupSigninTitle(this.props.language)}
                                </a>
                                <button id='kt_login_signin_submit' className='btn btn-primary btn-elevate kt-login__btn-primary' onClick={(event) => this.onSubmitButtonPressed(event)}>
                                    {Language.renderSignupSubmitTitle(this.props.language)}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};

const mapStateToProps = (state) => ({
    language: state.appLanguage.current,
    theme: state.appTheme.current
});

const mapDispatchToProps = (dispatch) => ({
    changeAuthenticatedData: (newAuthenticatedData) => dispatch(changeAuthenticatedData(newAuthenticatedData))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignupComponent));
