import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Language from '../languages/';
import Style from '../styles/';

import * as routeConstant from 'constant/route';

import InformationComponent from './information';
import SigninComponent from './signin';
import SignupComponent from './signup';
import ForgotComponent from './forgot';

class AuthenticationComponent extends React.Component {

    constructor(props) {
        super(props);
    };


    render() {
        return (
            <div className='kt-grid kt-grid--ver kt-grid--root'>
                <link rel='stylesheet' type='text/css' href={`${process.env.PUBLIC_URL}/assets/libs/metronic/css/pages/login/login-1.min.css`}></link>
                <style>{Style[this.props.theme]}</style>
                <div className='kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1' id='kt_login'>
                    <div className='kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile'>
                        <InformationComponent />
                        {
                            this.props.match.params.page === routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNIN && (
                                <SigninComponent />
                            )
                        }
                        {
                            this.props.match.params.page === routeConstant.ROUTE_NAME_AUTHENTICATION_SIGNUP && (
                                <SignupComponent />
                            )
                        }
                        {
                            this.props.match.params.page === routeConstant.ROUTE_NAME_AUTHENTICATION_FORGOT && (
                                <ForgotComponent />
                            )
                        }
                    </div>
                </div>
            </div>
        );
    };
};

const mapStateToProps = (state) => ({
    language: state.appLanguage.current,
    theme: state.appTheme.current
});

const mapDispatchToProps = (dispatch) => ({

});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AuthenticationComponent));