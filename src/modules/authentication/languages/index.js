import * as languageConstant from 'constant/language'

import * as vn from './vn'

class Language {

    static Authen(language) {
        switch (language) {
            case languageConstant.LANGUAGE_TYPE_VN:
                return vn.TEXT_AUTHEN
            default:
                break
        }
    }

    static Login(language) {
        switch (language) {
            case languageConstant.LANGUAGE_TYPE_VN:
                return vn.TEXT_LOGIN
            default:
                break
        }
    }

    static Signup(language) {
        switch (language) {
            case languageConstant.LANGUAGE_TYPE_VN:
                return vn.TEXT_SIGNUP
            default:
                break
        }
    }
}

export default Language